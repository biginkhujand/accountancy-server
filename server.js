﻿const mysql = require('mysql');
const express = require('express');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const salt = bcrypt.genSaltSync(10);

var con = mysql.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password: '',
    database: 'accountancy'
});

var app = express();

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', '*');
    res.header('Access-Control-Allow-Methods', '*');
    next();
});

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

con.connect(function (err) {
    if (!err) {
        var sql = `CREATE TABLE IF NOT EXISTS users (
            id INT AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(25),
            email VARCHAR(255) NOT NULL,
            password VARCHAR(255) NOT NULL,
            bill DECIMAL(5,2) NULL,
            created_at DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
            updated_at DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
            UNIQUE (email)
        )`;
        con.query(sql, function (err, result) {
            if (err) throw err;
        });
        var sql = `CREATE TABLE IF NOT EXISTS events (
            id INT AUTO_INCREMENT PRIMARY KEY,
            type ENUM('outcome','income') DEFAULT 'outcome',
            amount INT(11) NOT NULL,
            description VARCHAR(60),
            user_id INT NOT NULL,
            category_id INT NOT NULL,
            created_at DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
            updated_at DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP
        )`;
        con.query(sql, function (err, result) {
            if (err) throw err;
        });
        var sql = `CREATE TABLE IF NOT EXISTS categories (
            id INT AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(25) NOT NULL,
            capacity INT(11) NOT NULL,
            user_id INT NOT NULL,
            created_at DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
            updated_at DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP
        )`;
        con.query(sql, function (err, result) {
            if (err) throw err;
        });
        console.log('Database is connected...');
    } else {
        console.log('Error connecting database...');
    }
});


// Auth
app.post('/signup', function (req, res) {
    var name = req.body.name,
        email = req.body.email,
        password = req.body.password,
        hash = bcrypt.hashSync(password, salt);

    var sql = "INSERT INTO users (name, email, password) VALUES ('" + name + "', '" + email + "', '" + hash + "')";
    con.query(sql, function (err, rows, fields) {
        if (!err) {
            res.send({
                success: true,
                message: {type: 'success', text: 'Успешная регистрация!'}
            });
        } else {
            res.send({
                success: false,
                message: {type: 'danger', text: 'Пользователь с таким email уже существует!'}
            });
        }
    });
});

app.post('/signin', function (req, res) {
    var email = req.body.email,
        password = req.body.password;

    var sql = "SELECT * FROM users WHERE email = '" + email + "'";
    con.query(sql, function (err, rows, fields) {
        if (!err && rows.length > 0) {
            user = rows[0];
            hash = user.password;
            result = bcrypt.compareSync(password, hash);
            if (!result) {
                res.send({
                    success: false,
                    message: {type: 'danger', text: 'Неверный пароль!'}
                });
            } else {
                res.send({
                    success: true,
                    message: {type: 'success', text: 'Добро пожаловать!'},
                    id: user.id,
                    name: user.name
                });
            }
        } else {
            console.log(err);
            res.send({
                success: false,
                message: {type: 'danger', text: 'Нет такого пользователя!'}
            });
        }
    });
});

// Users
app.get('/users/:id', function (req, res) {
    var id = req.params.id;

    var sql = "SELECT name, email FROM users WHERE id = '" + id + "'";
    con.query(sql, function (err, rows, fields) {
        if (!err && rows.length > 0) {
            res.send({
                success: true,
                message: {type: 'success', text: 'Пользователь успешно найден!'},
                user: rows[0]
            });
        } else {
            res.send({
                success: false,
                message: {type: 'danger', text: 'Нет такого пользователя!'}
            });
        }
    });
});

app.put('/users/:id', function (req, res) {
    var id = req.params.id,
        name = req.body.name,
        email = req.body.email,
        password = req.body.password,
        hash = bcrypt.hashSync(password, salt);

    var sql = "UPDATE users SET name = '" + name + "', email = '" + email + "', password = '" + hash + "' WHERE id = '" + id + "'";
    con.query(sql, function (err, rows, fields) {
        if (!err && rows.affectedRows > 0) {
            res.send({
                success: true,
                message: {type: 'success', text: 'Данные пользователя успешно редактированы!'}
            });
        } else {
            res.send({
                success: false,
                message: {type: 'danger', text: 'Ошибка редактирования!'}
            });
        }
    });
});

app.delete('/users/:id', function (req, res) {
    var id = req.params.id;

    var sql = "DELETE FROM users WHERE id = '" + id + "'";
    con.query(sql, function (err, rows, fields) {
        if (!err && rows.affectedRows > 0) {
            res.send({
                success: true,
                message: {type: 'success', text: 'Пользователь успешно удален!'}
            });
        } else {
            res.send({
                success: false,
                message: {type: 'danger', text: 'Ошибка удаления!'}
            });
        }
    });
});

// Bill
app.get('/users/:id/bill', function (req, res) {
    var id = req.params.id;

    var sql = "SELECT bill FROM users WHERE id = '" + id + "'";
    con.query(sql, function (err, rows, fields) {
        if (!err && rows.length > 0) {
            res.send({
                success: true,
                message: {type: 'success', text: 'Счет пользователя найден!'},
                bill: rows[0].bill
            });
        } else {
            res.send({
                success: false,
                message: {type: 'danger', text: 'Нет такого пользователя!'}
            });
        }
    });
});

app.put('/users/:id/bill', function (req, res) {
    var id = req.params.id,
        bill = req.body.bill;
    var sql = "UPDATE users SET bill = '" + bill + "' WHERE id = '" + id + "'";
    con.query(sql, function (err, rows, fields) {
        if (!err && rows.affectedRows > 0) {
            res.send({
                success: true,
                message: {type: 'success', text: 'Счет пользователя успешно редактирован!'},
            });
        } else {
            res.send({
                success: false,
                message: {type: 'danger', text: 'Ошибка редактирования!'}
            });
        }
    });
});

// Categories
app.get('/users/:id/categories', function (req, res) {
    var id = req.params.id;

    var sql = "SELECT * FROM categories WHERE user_id = '" + id + "'";
    con.query(sql, function (err, rows, fields) {
        if (!err && rows.length > 0) {
            res.send({
                success: true,
                message: {type: 'success', text: 'Найдены все существующие категории!'},
                categories: rows
            });
        } else {
            res.send({
                success: false,
                message: {type: 'danger', text: 'У вас нет ни одной категории!'}
            });
        }
    });
});

app.get('/users/:id/categories/:catId', function (req, res) {
    var id = req.params.id,
        catId = req.params.catId;

    var sql = "SELECT * FROM categories WHERE user_id = '" + id + "' AND id = '" + catId + "'";
    con.query(sql, function (err, rows, fields) {
        if (!err && rows.length > 0) {
            res.send({
                success: true,
                message: {type: 'success', text: 'Категория найдена!'},
                category: rows[0]
            });
        } else {
            res.send({
                success: false,
                message: {type: 'danger', text: 'Нет такой категории!'}
            });
        }
    });
});

app.post('/users/:id/categories', function (req, res) {
    var id = req.params.id,
        name = req.body.name,
        capacity = req.body.capacity;

    var sql = "INSERT INTO categories (name, capacity, user_id) VALUES ('" + name + "', '" + capacity + "', '" + id + "')";
    con.query(sql, function (err, rows, fields) {
        if (!err) {
            res.send({
                success: true,
                message: {type: 'success', text: 'Категория успешно добавлена!'}
            });
        } else {
            res.send({
                success: false,
                message: {type: 'danger', text: 'Ошибка добавления!'}
            });
        }
    });
});

app.put('/users/:id/categories/:catId', function (req, res) {
    var id = req.params.id,
        catId = req.params.catId,
        name = req.body.name,
        capacity = req.body.capacity;

    var sql = "UPDATE categories SET name = '" + name + "', capacity = '" + capacity + "' WHERE id = '" + catId + "' AND user_id = '" + id + "'";
    con.query(sql, function (err, rows, fields) {
        if (!err && rows.affectedRows > 0) {
            res.send({
                success: true,
                message: {type: 'success', text: 'Категория успешно редактирована!'},
                category: {catId, name, capacity}
            });
        } else {
            res.send({
                success: false,
                message: {type: 'danger', text: 'Ошибка редактирования!'}
            });
        }
    });
});

app.delete('/users/:id/categories/:catId', function (req, res) {
    var id = req.params.id,
        catId = req.params.catId;

    var sql = "DELETE FROM categories WHERE id = '" + catId + "' AND user_id = '" + id + "'";
    con.query(sql, function (err, rows, fields) {
        if (!err && rows.affectedRows > 0) {
            res.send({
                success: true,
                message: {type: 'success', text: 'Категория успешно удалена!'},
            });
        } else {
            res.send({
                success: false,
                message: {type: 'danger', text: 'Ошибка удаления!'}
            });
        }
    });
});

// Events
app.get('/users/:id/events', function (req, res) {
    var id = req.params.id;

    var sql = "SELECT * FROM events WHERE user_id = '" + id + "'";
    con.query(sql, function (err, rows, fields) {
        if (!err && rows.length > 0) {
            res.send({
                success: true,
                message: {type: 'success', text: 'Найдены все существующие событии!'},
                events: rows
            });
        } else {
            res.send({
                success: false,
                message: {type: 'danger', text: 'У вас нет ни одного события!'}
            });
        }
    });
});

app.get('/users/:id/events/:eventId', function (req, res) {
    var id = req.params.id,
        eventId = req.params.eventId;

    var sql = "SELECT * FROM events WHERE user_id = '" + id + "' AND id = '" + eventId + "'";
    con.query(sql, function (err, rows, fields) {
        if (!err && rows.length > 0) {
            res.send({
                success: true,
                message: {type: 'success', text: 'Событие найдено!'},
                event: rows[0]
            });
        } else {
            res.send({
                success: false,
                message: {type: 'danger', text: 'Нет такого события!'}
            });
        }
    });
});

app.post('/users/:id/events', function (req, res) {
    var id = req.params.id,
        type = req.body.type,
        amount = req.body.amount,
        description = req.body.description,
        category_id = req.body.category_id;

    var sql = "INSERT INTO events (type, amount, description, user_id, category_id) VALUES ('" + type + "', '" + amount + "', '" + description + "', '" + id + "', '" + category_id + "')";
    con.query(sql, function (err, rows, fields) {
        if (!err) {
            res.send({
                success: true,
                message: {type: 'success', text: 'Событие успешно добавлено!'},
            });
        } else {
            res.send({
                success: false,
                message: {type: 'danger', text: 'Ошибка добавления!'}
            });
        }
    });
});

app.put('/users/:id/events/:eventId', function (req, res) {
    var id = req.params.id,
        eventId = req.params.eventId,
        type = req.body.type,
        amount = req.body.amount,
        description = req.body.description,
        category_id = req.body.category_id;

    var sql = "UPDATE events SET type = '" + type + "', amount = '" + amount + "', description = '" + description + "', category_id = '" + category_id + "' WHERE id = '" + eventId + "' AND user_id = '" + id + "'";
    con.query(sql, function (err, rows, fields) {
        if (!err) {
            res.send({
                success: true,
                message: {type: 'success', text: 'Событие успешно редактировано!'},
            });
        } else {
            res.send({
                success: false,
                message: {type: 'danger', text: 'Ошибка редактирования!'}
            });
        }
    });
});

app.delete('/users/:id/events/:eventId', function (req, res) {
    var id = req.params.id,
        eventId = req.params.eventId;

    var sql = "DELETE FROM events WHERE id = '" + eventId + "' AND user_id = '" + id + "'";
    con.query(sql, function (err, rows, fields) {
        if (!err && rows.affectedRows > 0) {
            res.send({
                success: true,
                message: {type: 'success', text: 'Событие успешно удалено!'},
            });
        } else {
            res.send({
                success: false,
                message: {type: 'danger', text: 'Ошибка удаления!'}
            });
        }
    });
});

app.listen(3000);